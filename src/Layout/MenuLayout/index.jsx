import React ,{Component} from 'react'
import {Menu, Icon, Container} from 'semantic-ui-react'

export default class MenuLayout extends Component {
    state = {activeItem: '홈'}

    handleItemClick = (e, { name }) => this.setState({ activeItem: name })

    render() {
        const { activeItem } = this.state    
        return(
            <div className="menu_wrap">
                <Menu pointing secondary  className="menu_style">
                <Menu.Item className="category">
                    <Icon className="align justify"/>
                    카테고리
                </Menu.Item>
                <Menu.Item
                    name='홈'
                    active={activeItem === '홈'}
                    onClick={this.handleItemClick}>
                    홈
                </Menu.Item>
                <Menu.Item
                    name='가전'
                    active={activeItem === '가전'}
                    onClick={this.handleItemClick}>
                    가전
                </Menu.Item>
                <Menu.Item
                    name='패션/잡화'
                    active={activeItem === '패션/잡화'}
                    onClick={this.handleItemClick}>
                    패션/잡화
                </Menu.Item>
                <Menu.Item
                    name='뷰티'
                    active={activeItem === '뷰티'}
                    onClick={this.handleItemClick}>
                    뷰티
                </Menu.Item>
            </Menu>
        </div>
        )
    }
}
