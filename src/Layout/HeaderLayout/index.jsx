import React ,{Component} from 'react'
import { Grid, Item, Dropdown, Header, Container,Image ,Menu, Icon} from 'semantic-ui-react'
import MenuLayout from '../MenuLayout'

class HeaderLayout extends Component {

    render(){ 
        const {children} = this.props;   
        
        const options = [
            { key: 1, text: 'Customer', value: 1 },
            { key: 2, text: 'Manager', value: 2 },
          ]

        return(
            <>
            <Container>
                <Grid  className="header_wrap">
                    <Grid.Row>
                        <Grid.Column width={12}>
                            <Header>Namoon<span>Mall</span></Header>
                        </Grid.Column>
                        <Grid.Column width={3} textAlign="right"> 
                            <Item className="mgr10">
                                <p><span clasName="bold">홍길동</span>님</p>
                                <Dropdown  text='Customer' options={options} />
                            </Item>
                            <Image src="/images/icons/icon-profile.svg " verticalAlign="top"/>
                        </Grid.Column>
                        <Grid.Column width={1}>
                            <Image src="/images/icons/icon-cart.svg" inline/>
                            <p>장바구니</p>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
            <Container className="menu_wrap" fluid>
                <MenuLayout></MenuLayout>
            </Container>
            <main>
                {children}
            </main>
            </>
        )
    }
}

export default HeaderLayout;