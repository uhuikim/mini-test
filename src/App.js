import React from 'react';
import HeaderLayout from './Layout/HeaderLayout'
import ProductList from'./views/ProductList';
import ProductDetail from "./views/ProductDetail"

function App() {
  return (
    <HeaderLayout><ProductDetail/></HeaderLayout>
  );
}
export default App;
