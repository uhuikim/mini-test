import React, {Component} from 'react';
import { Grid, Container ,Image, Item, Button, Input, Divider, Tab} from 'semantic-ui-react'

export default class ProductDetail extends Component {
    render() {
        const panes = [
            { menuItem: '상세설명', render: () => <Tab.Pane><Image src="images/temp/pd-detail01.png"/></Tab.Pane> },
            { menuItem: '혜택/배송', render: () => <Tab.Pane>혜택/배송</Tab.Pane> },
            { menuItem: '상품평', render: () => <Tab.Pane>상품평</Tab.Pane> },
            { menuItem: 'Q&A', render: () => <Tab.Pane>Q&A</Tab.Pane> },
          ]
        return(
            <>
                <Container className="product_purchase">
                    <Grid>
                        <Grid.Row columns={2}>
                            <Grid.Column className="image_wrap">
                                <Image as="div" src="/images/temp/product-01.png" size="large" centered/>
                            </Grid.Column>
                            <Grid.Column>
                                <Item>
                                    <Item.Content>
                                        <Item.Header>[BOSS]보스 사운드링크 리볼브 + 블루투스 스피커 SoundLink Revolve + Bluetooth speaker</Item.Header>
                                        <Item.Description><span>150,000</span>원</Item.Description>
                                        <Item.Extra className="qty">
                                            <label className="mgr30">수량</label>
                                            <Input>
                                                <Button icon="minus" basic className="minus"/>
                                                <Input placeholder='1' />
                                                <Button icon="plus" basic className="plus"/>
                                            </Input>
                                        </Item.Extra>
                                    </Item.Content>
                                </Item>
                                <Divider/>
                                <div className="total_price">
                                    <p>총 금액</p>
                                    <p className="price">150,000<span>원</span></p>
                                </div>
                                <Button.Group>
                                    <Button basic icon="heart outline"/>
                                    <Button content="장바구니" className="second_btn" size="large" basic/>
                                    <Button content="바로구매" primary size="large"/>
                                </Button.Group>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Container>
                <Container className="product_tap">
                    <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
                </Container>
            </>
        )
    }
}