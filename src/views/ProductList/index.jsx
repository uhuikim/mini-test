import React ,{Component} from 'react';
import {Grid,Item,Image, Container} from 'semantic-ui-react'

export default class ProductList extends Component {
    render(){
        return(
            <Container>
                <div className="content_wrap">
                <Grid>
                    <Grid.Row columns={5}>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-01.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-02.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-03.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-04.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-05.png'
                                    
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row columns={5}>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-01.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-02.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-03.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-04.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-05.png'
                                    
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row columns={5}>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-01.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-02.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-03.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-04.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-05.png'
                                    
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row columns={5}>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-01.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-02.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-03.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-04.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-05.png'
                                    
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row columns={5}>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-01.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-02.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-03.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-04.png'
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                        <Grid.Column>
                            <Item>
                                <Image
                                    as='div'
                                    size='medium'
                                    src='/images/temp/product-05.png'
                                    
                                />
                                <Item.Content>
                                    <Item.Header>상품명 product title</Item.Header>
                                    <Item.Description> 000<span>원</span></Item.Description>
                                </Item.Content>
                            </Item>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        </Container>
        )
    }
}
